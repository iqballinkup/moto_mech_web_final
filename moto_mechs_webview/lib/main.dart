import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:url_launcher/url_launcher.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();

  if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
  }
  runApp(MaterialApp(debugShowCheckedModeBanner: false, home: MyApp()));
}

Drawer myDrawer({required BuildContext context}) {
  return Drawer(
    child: ListView(
      children: [
        DrawerHeader(
          decoration: BoxDecoration(
            color: Colors.blue,
            image: DecorationImage(
              image: AssetImage("images/bike1.jpg"),
              fit: BoxFit.fill,
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                bottom: 8.0,
                left: 4.0,
                child: Text(
                  "Moto Mechs BD",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              )
            ],
          ),
        ),
        ListTile(
          leading: Icon(Icons.app_registration),
          title: Text("User Registration"),
          dense: true,
          visualDensity: VisualDensity(vertical: -4),
          onTap: () {},
        ),
        Divider(thickness: 1.0),
        ListTile(
          leading: Icon(Icons.login),
          dense: true,
          visualDensity: VisualDensity(vertical: -4),
          title: Text("User Login"),
          onTap: () {},
        ),
        Divider(thickness: 1.0),
        ListTile(
          leading: Icon(Icons.dashboard),
          dense: true,
          visualDensity: VisualDensity(vertical: -4),
          title: Text("User Dashboard"),
          onTap: () {},
        ),
        Divider(thickness: 1.0),
        ListTile(
          leading: Icon(Icons.book_online),
          dense: true,
          visualDensity: VisualDensity(vertical: -4),
          title: Text("Booking Request"),
          onTap: () {},
        ),
        Divider(thickness: 1.0),
        ListTile(
          leading: Icon(Icons.list),
          dense: true,
          visualDensity: VisualDensity(vertical: -4),
          title: Text("Booking Request List"),
          onTap: () {},
        ),
        Divider(thickness: 1.0),
        ListTile(
          leading: Icon(Icons.line_style),
          dense: true,
          visualDensity: VisualDensity(vertical: -4),
          title: Text("Approval List"),
          onTap: () {},
        ),
        Divider(thickness: 1.0),
        ListTile(
          leading: Icon(Icons.message),
          dense: true,
          visualDensity: VisualDensity(vertical: -4),
          title: Text("Message to Admin"),
          onTap: () {},
        ),
        Divider(thickness: 1.0),
        ListTile(
          leading: Icon(Icons.contact_mail),
          dense: true,
          visualDensity: VisualDensity(vertical: -4),
          title: Text("Contact Us Page"),
          onTap: () {},
        ),
        Divider(thickness: 1.0),
        ListTile(
          leading: Icon(Icons.info),
          dense: true,
          visualDensity: VisualDensity(vertical: -4),
          title: Text("Service Center Information Page"),
          onTap: () {},
        ),
        Divider(thickness: 1.0),
        ListTile(
          leading: Icon(Icons.person),
          dense: true,
          visualDensity: VisualDensity(vertical: -4),
          title: Text("Customer Profile and Picture Update"),
          onTap: () {},
        ),
        Divider(thickness: 1.0),
        ListTile(
          leading: Icon(Icons.blinds_closed),
          dense: true,
          visualDensity: VisualDensity(vertical: -4),
          title: Text("Bill information Page"),
          onTap: () {},
        ),
        Divider(thickness: 1.0),
        ListTile(
          leading: Icon(Icons.message),
          dense: true,
          visualDensity: VisualDensity(vertical: -4),
          title: Text("Message from admin"),
          onTap: () {},
        ),
      ],
    ),
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final GlobalKey webViewKey = GlobalKey();

  // bool pressAttention = false;
  int _selectedIndex = 0;

  InAppWebViewController? webViewController;
  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
          useShouldOverrideUrlLoading: true,
          mediaPlaybackRequiresUserGesture: false,
          preferredContentMode: UserPreferredContentMode.MOBILE),
      android: AndroidInAppWebViewOptions(
        useHybridComposition: true,
      ),
      ios: IOSInAppWebViewOptions(
        allowsInlineMediaPlayback: true,
      ));

  late PullToRefreshController pullToRefreshController;
  String url = "";
  double progress = 0;
  final urlController = TextEditingController();

  @override
  void initState() {
    super.initState();

    pullToRefreshController = PullToRefreshController(
      options: PullToRefreshOptions(
        color: Colors.blue,
      ),
      onRefresh: () async {
        if (Platform.isAndroid) {
          webViewController?.reload();
        } else if (Platform.isIOS) {
          webViewController?.loadUrl(
              urlRequest: URLRequest(url: await webViewController?.getUrl()));
        }
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(40.0),
          child: AppBar(
            title: Text("MotoMechs BD"),
            centerTitle: true,
            backgroundColor: Color.fromARGB(255, 129, 170, 240),
          ),
        ),
        drawer: myDrawer(context: context),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              _selectedIndex = 0;
            });
            print("Home button is tapped");
            webViewController?.loadUrl(
                urlRequest:
                    URLRequest(url: Uri.parse("https://motomechsbd.com/")));
          },
          backgroundColor: Color.fromARGB(255, 129, 170, 240),
          child: Icon(Icons.home),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: Column(children: <Widget>[
          Expanded(
            child: Stack(
              children: [
                InAppWebView(
                  key: webViewKey,
                  initialUrlRequest:
                      URLRequest(url: Uri.parse("https://motomechsbd.com/")),
                  initialOptions: options,
                  pullToRefreshController: pullToRefreshController,
                  onWebViewCreated: (controller) {
                    webViewController = controller;
                  },
                  onLoadStart: (controller, url) {
                    setState(() {
                      this.url = url.toString();
                      urlController.text = this.url;
                    });
                  },
                  androidOnPermissionRequest:
                      (controller, origin, resources) async {
                    return PermissionRequestResponse(
                        resources: resources,
                        action: PermissionRequestResponseAction.GRANT);
                  },
                  shouldOverrideUrlLoading:
                      (controller, navigationAction) async {
                    var uri = navigationAction.request.url!;

                    if (![
                      "http",
                      "https",
                      "file",
                      "chrome",
                      "data",
                      "javascript",
                      "about"
                    ].contains(uri.scheme)) {
                      if (await canLaunch(url)) {
                        // Launch the App
                        await launch(
                          url,
                        );
                        // and cancel the request
                        return NavigationActionPolicy.CANCEL;
                      }
                    }

                    return NavigationActionPolicy.ALLOW;
                  },
                  onLoadStop: (controller, url) async {
                    pullToRefreshController.endRefreshing();
                    setState(() {
                      this.url = url.toString();
                      urlController.text = this.url;
                    });
                  },
                  onLoadError: (controller, url, code, message) {
                    pullToRefreshController.endRefreshing();
                  },
                  onProgressChanged: (controller, progress) {
                    if (progress == 100) {
                      pullToRefreshController.endRefreshing();
                    }
                    setState(() {
                      this.progress = progress / 100;
                      urlController.text = this.url;
                    });
                  },
                  onUpdateVisitedHistory: (controller, url, androidIsReload) {
                    setState(() {
                      this.url = url.toString();
                      urlController.text = this.url;
                    });
                  },
                  onConsoleMessage: (controller, consoleMessage) {
                    print(consoleMessage);
                  },
                ),
                progress < 1.0
                    ? LinearProgressIndicator(value: progress)
                    : Container(),
              ],
            ),
          ),
          Container(
            height: 55,
            color: Color.fromARGB(255, 129, 170, 240),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      _selectedIndex = 1;
                    });
                    print("about button is tapped");
                    webViewController?.loadUrl(
                        urlRequest: URLRequest(
                            url:
                                Uri.parse("https://motomechsbd.com/about-us")));
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.info,
                        color:
                            _selectedIndex == 1 ? Colors.black54 : Colors.white,
                        size: 23,
                      ),
                      Text(
                        "About",
                        style: TextStyle(
                          color: _selectedIndex == 1
                              ? Colors.black54
                              : Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      _selectedIndex = 2;
                    });
                    print("service button is tapped");
                    webViewController?.loadUrl(
                        urlRequest: URLRequest(
                            url: Uri.parse("https://motomechsbd.com/service")));
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.home_repair_service,
                        color:
                            _selectedIndex == 2 ? Colors.black54 : Colors.white,
                        size: 23,
                      ),
                      Text(
                        "Services",
                        style: TextStyle(
                          color: _selectedIndex == 2
                              ? Colors.black54
                              : Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  width: 40.0,
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      _selectedIndex = 3;
                    });
                    print("gallery button is tapped");
                    webViewController?.loadUrl(
                        urlRequest: URLRequest(
                            url: Uri.parse("https://motomechsbd.com/gallery")));
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.image,
                        color:
                            _selectedIndex == 3 ? Colors.black54 : Colors.white,
                        size: 23,
                      ),
                      Text(
                        "Gallery",
                        style: TextStyle(
                          color: _selectedIndex == 3
                              ? Colors.black54
                              : Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      _selectedIndex = 4;
                    });
                    print("Contact button is tapped");
                    webViewController?.loadUrl(
                        urlRequest: URLRequest(
                            url: Uri.parse("https://motomechsbd.com/contact")));
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.phone,
                        color:
                            _selectedIndex == 4 ? Colors.black54 : Colors.white,
                        size: 23,
                      ),
                      Text(
                        "Contact",
                        style: TextStyle(
                          color: _selectedIndex == 4
                              ? Colors.black54
                              : Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ]));
  }
}
